#!/bin/bash

# pull or load dotnet sdk.
if [[ -e ~/docker/dotnet-sdk.tar ]]; then
    docker load -i ~/docker/dotnet-sdk.tar
else
    docker pull microsoft/dotnet:sdk
    mkdir -p ~/docker; docker save -o ~/docker/dotnet-sdk.tar microsoft/dotnet:sdk
fi

# pull or load dotnet 1.1.1-runtime.
if [[ -e ~/docker/dotnet-1-1-1-runtime.tar ]]; then
    docker load -i ~/docker/dotnet-1-1-1-runtime.tar
else
    docker pull microsoft/dotnet:1.1.1-runtime
    docker save -o ~/docker/dotnet-1-1-1-runtime.tar microsoft/dotnet:1.1.1-runtime
fi